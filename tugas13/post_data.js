import React, {useState,useEffect} from 'react';
import { StyleSheet, Text, View, Button, TextInput, Alert } from 'react-native';





export default function PostData() {
    const [author,setAuthor] = useState("");
    const [title,setTitle] = useState("");
    const [description,setDescription] = useState("");
    const [country,setCountry] = useState("");
    const [button,setButton] = useState("simpan");

    const base_url = "https://demoapi-hilmy.sanbercloud.com/api/news-created"
    const token_api = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZGVtb2FwaS1oaWxteS5zYW5iZXJjbG91ZC5jb21cL2FwaVwvbG9naW4iLCJpYXQiOjE2NjQzODc4OTYsImV4cCI6MTcyNDM4NzgzNiwibmJmIjoxNjY0Mzg3ODk2LCJqdGkiOiJ0aDJHWXZJeHNqR0l0NXRaIiwic3ViIjoyLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.4fVp39fSYtl-rVwc4N_mYaURnh7uBRt2xw_NifLIE2g";

    const submit = async() => {
        const data = {
            title: title, description : description, country: country, author: author 
        }
        console.log(data)
        try {
            const response = await fetch(base_url, {
                method: 'POST',
                headers: {
                    "Accept" : "application/json",
                    "Content-Type" : 'application/json',
                    "Authorization": `Bearer ${token_api}`
                },
                body:  JSON.stringify(data)
            })
            const json = await response.json()
            console.log("Data Berhasil Disimpan",json)
            setTitle(""),
            setCountry(""),
            setDescription(""),
            setAuthor("")
            Alert("Data Berhasil Disimpan")

        } catch (error) {
            console.log(error)
        }
    }
  return (
    <View style={styles.container}>
        <View style={styles.headers}> 
            <Text>Post Data</Text>
        </View>
        <View style={styles.content}>
            <TextInput
                placeholder='Masukkan Author'
                value={author}
                onChangeText={(value)=>setAuthor(value)}
                style={styles.input}
            />
            <TextInput
                placeholder='Masukkan Title'
                value={title}
                onChangeText={(value)=>setTitle(value)}
                style={styles.input}
            />
            <TextInput
                placeholder='Masukkan Description'
                value={description}
                onChangeText={(value)=>setDescription(value)}
                style={styles.input}
            />
             <TextInput
                placeholder='Masukkan Country'
                value={country}
                onChangeText={(value)=>setCountry(value)}
                style={styles.input}
            />
            <Button
                title={button}
                onPress={submit}
            />
            

        </View>

    </View>

  )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'white',
    },
    headers: {
        paddingTop: 50,
        paddingHorizontal: 16,
        backgroundColor: "white",
        alignItems: 'center'
    },
    content : {
        paddingHorizontal:  10
    },
    input: {
        borderWidth: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderRadius: 6,
        marginBottom: 10,
        marginTop: 10
    }
})