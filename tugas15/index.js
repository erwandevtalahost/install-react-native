import {NavigationContainer} from "@react-navigation/native";
import { AuthProvider } from "./context/AuthContext";


import RootStackNavigator from "./routes/RootStackNavigator";




export default function Tugas15()  {
    return (
        <AuthProvider>
            <NavigationContainer>
                <RootStackNavigator/>
            </NavigationContainer>
        </AuthProvider>

    )
}