import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";

///===== IMPORT About Screen

import AboutScreen from "../screen/AboutScreen";
import LoginScreen from "../screen/LoginScreen";


const Stack = createNativeStackNavigator();

export default function RootStackNavigator()  {
    const {authState} = useContext(AuthContext);

    return (
        <Stack.Navigator>
            {authState.isSignedIn ? (
                <Stack.Screen name="AboutScreen" component={AboutScreen}/>
            ) : (
                <Stack.Screen name="LoginScreen" component={LoginScreen}/>
            )}
        </Stack.Navigator>
    )

};



