import { useContext } from "react";
import { View, Button, StyleSheet, Text } from "react-native";
import { AuthContext} from "../context/AuthContext";



export default function AboutScreen()  {
    const {authState, signOut} = useContext(AuthContext);

    return (
        <View style={styles.container}>
            <Text>AboutScreen</Text>
            <View>
                <Text>Hello, {authState.email}</Text>
            </View>
            <Button title="SignOut" onPress={signOut}></Button>

        </View>
    );
};




const styles = StyleSheet.create({
    container: {
        padding: 20
    }
});