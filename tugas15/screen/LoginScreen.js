import { useContext,useState } from "react";
import { Button, StyleSheet,Text,TextInput,View } from "react-native";
import { AuthContext } from "../context/AuthContext";




export default function LoginScreen() {
    const {signIn, authState, ...other} = useContext(AuthContext);
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");

    return (
        <View style={styles.container}>
            <Text>LoginScreen</Text>
            <View>
                <TextInput
                    style={styles.formInputText}
                    value={email}
                    onChangeText={setEmail}
                    placeholder="Input Email"
                />

                <TextInput
                    style={styles.formInputText}
                    value={password}
                    onChangeText={setPassword}
                    placeholder="Input Password"
                    secureTextEntry
                />
            </View>
            <View>
                <Button title={"Login"} onPress={() => signIn({ email })}/>

            </View>
        </View>
    )
}





const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: "white",
        padding: 20,
    },
    formInputText: {
        alignItems: "center",
        borderColor: "#eaeaea",
        borderRadius: 4,
        borderWidth: 1,
        color: "#7d7d7d",
        flexDirection: "row",
        fontSize: 18,
        height: 54,
        justifyContent: "center",
        marginVertical: 8,
        paddingHorizontal: 10
    },
})