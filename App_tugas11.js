import React from 'react';


import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Loginscreen from './tugas10/loginscreen';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


const Stack = createNativeStackNavigator();


const App = () =>{
  return (
    <NavigationContainer>
      <Stack.Navigator>
          <Stack.Screen component={Main} name="Main" options={{headerShown: false}} />
          <Stack.Screen component={Home} name="Home" />
      </Stack.Navigator>

    </NavigationContainer>
  );
}



const Main =({navigation}) => {
  return (
    <SafeAreaView style={{
      flex:1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#fff'
    }}>
      <View>
        <Text style={{
          fontSize:30,
          fontWeight: 'bold',
          color: '#20315f'
        }}>GAME ON</Text>
      </View>
      <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}></View>
      <TouchableOpacity
        onPress={() => navigation.navigate('Home')}
        style={{
          backgroundColor: '#AD40AF',
          padding:20,
          width: '90%',
          borderRadius:5,
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 50
      
      
          }}
      
      >
        <Text style={{fontWeight: 'bold',fontSize:18,color:'#fff'}}>Lets Begin</Text>
        <MaterialIcons name='arrow-forward-ios' size={22} color='#fff'>

        </MaterialIcons>

      </TouchableOpacity>

    </SafeAreaView>
  );
}


const Home = () => {
  return (
    <View style={{flex:1,justifyContent: 'center', alignItems:'center'}}>
      <Text> Home Screen</Text>

    </View>


  );

}


export default App;


