import React,{ useState,useEffect } from 'react';

import { FlatList,ActivityIndicator,Text,View,Image,TouchableOpacity } from 'react-native';




export default function GetData() {
    const base_url = "https://reqres.in/api/users?page=2";
    const token_api = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZGVtb2FwaS1oaWxteS5zYW5iZXJjbG91ZC5jb21cL2FwaVwvbG9naW4iLCJpYXQiOjE2NjQyNTM2MTUsImV4cCI6MTcyNDI1MzU1NSwibmJmIjoxNjY0MjUzNjE1LCJqdGkiOiJCbGlEdEdXbHZ1V1N4WVZjIiwic3ViIjoyLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.Ruwf8XtH5Uh1zFk_WuVRerAR5ualPw0Z6F_qVL0BmKE";
    const url_delete = "https://demoapi-helmy.sanbercloud.com/api/news-remove"; 
    const [isloading,setLoading] = useState(true)
    const [data,setData]= useState([])

    const getNews = async () => {
        try {
            const response = await fetch(base_url, {
                headers: {
                    "Authorization": `Bearer ${token_api}`
                },
                

            });

            const json = await response.json();
            setData(json.data)

        } catch (error) {
            console.error(error)
        } finally {
            setLoading(false);
        }
    }

    onDeleteEvent = async(item) =>{
        try {
            const response = await fetch(`${url_delete}/${item.id}`,{
                method : 'DELETE',
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "Authorization" : `Bearer ${token_api}`
                },

            })
            const json = await response.json();
            console.log("Data Berhasil Dihapus, json")
            getNews()

        } catch (error) {
            console.log("errrornya", error)
        }
    }

    useEffect(() =>{
        getNews();
    },[]);
  return (
    <View style={{flex:1, padding: 24 , backgroundColor: "white"}}>
        {isloading ? <ActivityIndicator/> : (
            <FlatList
                data={data}
                keyExtractor={({id},index) => id}
                renderItem={({item})=> (

                    <>
                    <TouchableOpacity onPress={()=>onDeleteEvent(item)}>
                        <Text style={{color: "red"}}>X Delete</Text>
                    </TouchableOpacity>
                    <View style={{
                        flex:1,
                        flexDirection: "column",
                        borderWidth: 0.5,
                        marginBottom: 5,
                        paddingLeft: 10,
                        paddingBottom: 10,
                        paddingTop:10

                    }}>

                        <Text>{item.id}</Text>
                        <Text>{item.email}</Text>
                        <Text>{item.first_name}</Text>
                        <Image
                            style={{height:20,width:20,resizeMode: "contain"}}
                            source={{uri: item.avatar}}

                        />



                    </View>
                    </>
            
            
            
                )}


            />

        )}

    </View>
  )
}
