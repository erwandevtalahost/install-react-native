import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Telegram from './tugas/tugas8/Telegram';


export default function App() {
  return (
    <Telegram/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
