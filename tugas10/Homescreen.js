import React,{useState} from "react";
import { StyleSheet,Text,View,Image,SafeAreaView,ScrollView } from "react-native";






export default function Homescreen() {
  return (
    <SafeAreaView style={styles.container}>
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.title}>Tentang Saya</Text>
                <View style={styles.subtitle}>
                    <Image
                        style={styles.imageLogo}
                        source={require("../asset/account-logo.png")}
                    />

                </View>
                <>
                    <Text style={styles.name}>Erwansyah</Text>
                    <Text style={styles.jobs}>React Native Developer</Text>
                </>
                <View style={styles.portofolio}>
                    <View style={styles.contentPortofolio}>
                        <Text>Portofolio</Text>
                        <View style={styles.contentSkill}>
                            <View style={styles.subContentSkill}>
                                <Image source={require("../asset/gitlab.png")}/>
                                <Text>Gitlab</Text>

                            </View>
                            <View style={styles.subContentSkill}>
                                <Image source={require("../asset/github.png")}/>
                                <Text>Github</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.socialMedia}>
                    <View style={styles.contentMediaSosial}>
                        <Text>Hubungi Saya</Text>
                        <View style={styles.contentMediaSosialDown}>
                            <View style={styles.subContentMediaSosialDown}>
                                <>
                                    <Image source={require("../asset/logo-facebook.png")}/>
                                    <Text>Gitlab</Text>
                                </>
                                <>
                                    <Image source={require("../asset/logo-twitter.png")} />
                                    <Text>Twitter</Text>
                                </>
                                <>
                                    <Image source={require("../asset/logo-instagram.png")}/>
                                    <Text>Instagram</Text>
                                </>

                            </View>

                        </View>
                    </View>


                </View>

            </View>

        </ScrollView>

    </SafeAreaView>
  )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: "white"
    },
    scrollView: {
        backgroundColor: 'pink',
        marginHorizontal: 20
    },
    imageLogo: {
        height: 136,
        width: 136,
        resizeMode: "stretch",
        marginBottom: 20,
        alignSelf: "center",
        marginTop: 20
    },
    title: {
        fontSize: 36,
        fontWeight: "700",
        marginBottom: 20,
    },
    subtitle: {
        backgroundColor: "grey",
        height: 200,
        width: 200,
        borderRadius: 100,
    },
    name: {
        fontSize: 24,
        fontWeight: "700",
        color: "blue"
    },
    jobs: {
        fontSize: 16,
        fontWeight: "400",
        color: "blue",
    },
    portofolio: {
        backgroundColor: "#EFEFEF",
        height: 140,
        width: 300
    },
    contentPortofolio: {
        padding: 5
    },
    contentSkill: {
        padding: 10,
        flexDirection: "row",
        justifyContent: "space-around",
        height: 90,
        marginTop: 10
    },
    subContentSkill: {
        flexDirection: "column",
        paddingBottom: 5
    },
    socialMedia: {
        marginTop: 10,
        backgroundColor: "#EFEFEF",
        height: 300,
        width: 300,
    },
    contentMediaSosial: {
        padding: 8
    },
    contentMediaSosialDown: {
        flexDirection: "column",
        justifyContent: "space-around",
    },
    subContentMediaSosialDown: {
        alignSelf: "center",
        marginTop: 10
    }
})