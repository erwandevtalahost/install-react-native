import React, { useState } from 'react'
import { StyleSheet,Text,View, Image, TextInput,TouchableOpacity } from 'react-native'

export default function Loginscreen() {
    const [username,setUsername] = React.useState("Username");
    const [password,setPassword] = React.useState("Password");
    const [count,setCount] = useState(0);
    const onPress = () => setCount(prevcount => prevcount + 1);
    console.log(onPress)
  return (
    <View style={styles.container}>
        <Image
            style={styles.imageLogo}
            source={require("../asset/logo.png")}
        />
        <View style={styles.countContainer}></View>
        <Text style={styles.textLabel}>Username</Text>
        <TextInput
            style= {styles.inputText}
            onChangeText={setUsername}
            value={username}
         />
         <Text>Password</Text>
         <TextInput
            style = {styles.inputText}
            onChangeText={setPassword}
            value={password}
        />
        <TouchableOpacity
            style = {styles.button}
            onPress={onPress}>
                <Text style={styles.titleButton}>Press Here</Text>

        </TouchableOpacity>

    </View>    
  )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    imageLogo: {
        height: 100,
        width: 100,
        marginBottom: 20
    },
    inputText: {
        height: 50,
        width: 300,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius: 5
    },
    button: {
        marginTop: 50,
        height: 50,
        width: 300,
        backgroundColor: "blue",
        borderRadius: 5,
        alignItems: "center",
        padding: 10
    },
    textLabel : {
        left: "10px"
    },
    titleButton: {
        color: "white"
    },
    countContainer: {
        alignItems: "center",
        padding: 10
    }
})